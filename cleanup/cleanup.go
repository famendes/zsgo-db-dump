package cleanup

import (
	"io"
	"os"
)

func DeleteFiles(files []string) error {

	isEmpty, err := isEmpty("db_dumps")
	if err != nil {
		return err
	}
	if !isEmpty {
		for _, file := range files {
			err := os.Remove(file)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func isEmpty(name string) (bool, error) {
	f, err := os.Open(name)
	if err != nil {
		return false, err
	}
	defer f.Close()

	_, err = f.Readdirnames(1)
	if err == io.EOF {
		return true, nil
	}
	return false, err
}
