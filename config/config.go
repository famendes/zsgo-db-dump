package config

import (
	"log"
	"os"
	"strconv"

	"github.com/joho/godotenv"
)

var (
	Port            = 0
	Env             = ""
	MailHost        = ""
	MailPort        = ""
	MailUsername    = ""
	MailPassword    = ""
	MailFromAddress = ""
	MailFromName    = ""
)

func Load() {
	var err error

	if godotenv.Load(); err != nil {
		log.Fatal(err)
	}
	loadCommon()
	loadMailData()
}

func loadCommon() {
	var err error
	Env = os.Getenv("ENV")

	Port, err = strconv.Atoi(os.Getenv("API_PORT"))
	if err != nil {
		Port = 8001
	}
}

func loadMailData() {
	MailHost = os.Getenv("MAIL_HOST")
	MailPort = os.Getenv("MAIL_PORT")
	MailUsername = os.Getenv("MAIL_USERNAME")
	MailPassword = os.Getenv("MAIL_PASSWORD")
	MailFromAddress = os.Getenv("MAIL_FROM_ADDRESS")
	MailFromName = os.Getenv("MAIL_FROM_NAME")
}
