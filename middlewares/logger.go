package middlewares

import (
	"net/http"
	"zsgo-db-dump/enums"
	"zsgo-db-dump/logger"
)

func Log(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		standardLogger := *logger.NewLogger()
		standardLogger.Log(r, "", enums.LogAccess)
		next.ServeHTTP(w, r)
	})
}
