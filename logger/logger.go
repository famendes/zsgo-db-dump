package logger

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"runtime"
	"strings"
	"zsgo-db-dump/enums"

	"github.com/sirupsen/logrus"
)

type StandardLogger struct {
	*logrus.Logger
}

// passar caminho do ficheiro dos outros módulos
func NewLogger() *StandardLogger {
	var baseLogger = logrus.New()
	var standardLogger = &StandardLogger{baseLogger}
	standardLogger.Formatter = &logrus.TextFormatter{}
	return standardLogger
}

func (l *StandardLogger) Log(r *http.Request, msg, logType string) {
	var client string
	if r.Header.Get("X-Real-IP") == "" {
		if r.Header.Get("X-Forwarded-For") == "" {
			client = r.RemoteAddr
		} else {
			client = r.Header.Get("X-Forwarded-For")
		}
	} else {
		client = r.Header.Get("X-Real-IP")
	}

	keys := []string{"user-agent", "client", "route", "method", "protocol"}
	values := []interface{}{r.Header.Get("User-Agent"), client, r.RequestURI, r.Method, r.Proto}
	fields := addFields(keys, values)
	l.info(fields, msg, logType)
}

// passar args chave valor dinâmicos
func (l *StandardLogger) info(fields map[string]interface{}, msg, logType string) {
	file, err := os.OpenFile(fmt.Sprintf("logs/%s.log", logType), os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		log.Fatal(err)
	}
	l.Out = file

	if l.Level >= logrus.InfoLevel {
		entry := l.WithFields(fields)
		if logType == enums.LogError {
			entry.Data["caller"] = callerInfo(3)
			entry.Error(msg)
		} else {
			entry.Info(msg)
		}
	}
}

func addFields(keys []string, values []interface{}) map[string]interface{} {
	fields := make(map[string]interface{})

	for i, key := range keys {
		fields[key] = values[i]
	}
	return fields
}

// Comentar isto
func callerInfo(skip int) string {
	fn := ""
	pc, file, line, ok := runtime.Caller(skip)
	if !ok {
		file = "<???>"
		line = 1
	} else {
		funcname := runtime.FuncForPC(pc).Name()
		fn = funcname[strings.LastIndex(funcname, ".")+1:]
		slash := strings.LastIndex(file, "/")
		if slash >= 0 {
			file = file[slash+1:]
		}
	}
	return fmt.Sprintf("%s:%d@%s()", file, line, fn)
}
