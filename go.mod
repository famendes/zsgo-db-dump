module zsgo-db-dump

go 1.17

require (
	github.com/JamesStewy/go-mysqldump v0.2.2 // indirect
	github.com/felixge/httpsnoop v1.0.1 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/hanzoai/gochimp3 v0.0.0-20210305004051-da66ea724147 // indirect
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/jordan-wright/email v4.0.1-0.20210109023952-943e75fe5223+incompatible // indirect
	github.com/sirupsen/logrus v1.8.1 // indirect
	github.com/xhit/go-simple-mail/v2 v2.10.0 // indirect
	golang.org/x/crypto v0.0.0-20220126234351-aa10faf2a1f8 // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	golang.org/x/sys v0.0.0-20220114195835-da31bd327af9 // indirect
	golang.org/x/text v0.3.7 // indirect
)
