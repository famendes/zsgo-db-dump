package database

import (
	"database/sql"
	"fmt"
	"os"
	"zsgo-db-dump/config"

	_ "github.com/go-sql-driver/mysql"
)

func Connect(dbName string) (*sql.DB, error) {

	db, err := sql.Open("mysql", getConnectionString(dbName))
	if err != nil {
		return nil, err
	}

	if err := db.Ping(); err != nil {
		db.Close()
		return nil, err
	}

	return db, nil
}

func getConnectionString(dbName string) string {
	connectionString := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?charset=utf8&parseTime=True&loc=Local",
		os.Getenv(fmt.Sprintf("%s_DB_USERNAME", config.Env)),
		os.Getenv(fmt.Sprintf("%s_DB_PASSWORD", config.Env)),
		os.Getenv(fmt.Sprintf("%s_DB_HOST", config.Env)),
		os.Getenv(fmt.Sprintf("%s_DB_PORT", config.Env)),
		dbName,
	)

	return connectionString
}
