package compressor

import (
	"archive/zip"
	"fmt"
	"io"
	"os"
)

func Compress(fileName, nif, dbName string) (string, error) {
	// zipFileName := strings.Split(fileName, ".sql")
	zipFileName := fmt.Sprintf("db_dumps/zsgo-%s-%s", nif, dbName)

	newFile, err := os.Create(zipFileName + ".zip")
	if err != nil {
		return "", err
	}
	defer newFile.Close()

	zipit := zip.NewWriter(newFile)
	defer zipit.Close()

	zipfile, err := os.Open(fileName)
	if err != nil {
		return "", err
	}
	defer zipfile.Close()

	info, err := zipfile.Stat()
	if err != nil {
		return "", err
	}

	header, err := zip.FileInfoHeader(info)
	if err != nil {
		return "", err
	}

	header.Method = zip.Deflate

	writer, err := zipit.CreateHeader(header)
	if err != nil {
		return "", err
	}

	_, err = io.Copy(writer, zipfile)
	if err != nil {
		return "", err
	}
	return newFile.Name(), err
}
