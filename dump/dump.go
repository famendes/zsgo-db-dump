package dump

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"zsgo-db-dump/config"
)

func DumpDB(dbName string) (string, error) {
	cmd := exec.Command(
		"mysqldump",
		fmt.Sprintf("--port=%s", os.Getenv(fmt.Sprintf("%s_DB_PORT", config.Env))),
		fmt.Sprintf("--host=%s", os.Getenv(fmt.Sprintf("%s_DB_HOST", config.Env))),
		fmt.Sprintf("--user=%s", os.Getenv(fmt.Sprintf("%s_DB_USERNAME", config.Env))),
		fmt.Sprintf("--password=%s", os.Getenv(fmt.Sprintf("%s_DB_PASSWORD", config.Env))),
		"--default-character-set=utf8",
		"--single-transaction=TRUE",
		"--column-statistics=0",
		"--skip-triggers",
		"--protocol=tcp",

		dbName,
	)
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return "", err
	}

	if err := cmd.Start(); err != nil {
		return "", err
	}

	bytes, err := ioutil.ReadAll(stdout)
	if err != nil {
		return "", err
	}

	fileName := fmt.Sprintf("./db_dumps/%s.sql", dbName)
	err = ioutil.WriteFile(fileName, bytes, 0644)
	if err != nil {
		return "", err
	}
	return fileName, nil
}

//mysqldump.exe --defaults-file="C:\Users\FILIPE~1\AppData\Local\Temp\tmpef9jab7d.cnf"  --host=192.168.5.124 --port=3306 --default-character-set=utf8 --user=root --protocol=tcp --single-transaction=TRUE --column-statistics=0 --skip-triggers "8c12f8020a3a4a4697ace6844635bf81"
