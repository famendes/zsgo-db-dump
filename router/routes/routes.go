package routes

import (
	"net/http"
	"zsgo-db-dump/middlewares"

	"github.com/gorilla/mux"
)

type Route struct {
	URI      string
	Method   string
	Function func(http.ResponseWriter, *http.Request)
}

func Configure(r *mux.Router) *mux.Router {
	var routes []Route
	routes = append(routes, statusRoute)
	routes = append(routes, DumpSerialRoute)

	for _, route := range routes {
		r.HandleFunc(route.URI, route.Function).Methods(route.Method)
	}

	r.Use(middlewares.Log)

	return r
}
