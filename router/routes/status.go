package routes

import "zsgo-db-dump/controllers"

var statusRoute = Route{
	URI:      "/status",
	Method:   "GET",
	Function: controllers.GetAPIStatus,
}
