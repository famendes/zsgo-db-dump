package routes

import "zsgo-db-dump/controllers"

var DumpSerialRoute = Route{
	URI:      "/dump",
	Method:   "POST",
	Function: controllers.DumpSerial,
}
