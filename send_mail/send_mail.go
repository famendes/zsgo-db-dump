package sendmail

import (
	"bytes"
	"crypto/tls"
	"fmt"
	"html/template"
	"strconv"
	"strings"
	"time"
	"zsgo-db-dump/config"
	"zsgo-db-dump/models"

	mail "github.com/xhit/go-simple-mail/v2"
)

func SendEmail(companyData models.CompanyData, nif string, filePath string) error {

	fileName := strings.SplitAfter(filePath, "/")

	port, err := strconv.Atoi(config.MailPort)
	if err != nil {
		return err
	}

	server := mail.NewSMTPClient()

	// SMTP Server
	server.Host = config.MailHost
	server.Port = port
	server.Username = config.MailUsername
	server.Password = config.MailPassword
	server.Encryption = mail.EncryptionSSLTLS

	// Since v2.3.0 you can specified authentication type:
	// - PLAIN (default)
	// - LOGIN
	// - CRAM-MD5
	// server.Authentication = mail.AuthPlain

	// Variable to keep alive connection
	server.KeepAlive = false

	// Timeout for connect to SMTP Server
	server.ConnectTimeout = 10 * time.Second

	// Timeout for send the data and wait respond
	server.SendTimeout = 10 * time.Second

	// Set TLSConfig to provide custom TLS configuration. For example,
	// to skip TLS verification (useful for testing):
	server.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	// SMTP client
	smtpClient, err := server.Connect()

	if err != nil {
		return err
	}

	// New email simple html with inline and CC
	email := mail.NewMSG()
	email.SetFrom(fmt.Sprintf("%s <%s>", config.MailFromName, config.MailFromAddress)).
		AddTo(companyData.Email...).
		SetSubject(fmt.Sprintf("Dados de Faturação: %s", nif)).
		Attach(&mail.File{FilePath: filePath, Name: fileName[1], Inline: false})

	emailBytes, err := getTemplate()
	if err != nil {
		return err
	}
	email.SetBodyData(mail.TextHTML, []byte(emailBytes))

	// Call Send and pass the client
	err = email.Send(smtpClient)
	if err != nil {
		return err
	}

	return nil
}

func getTemplate() ([]byte, error) {

	t, err := template.ParseFiles("templates/template.html")
	if err != nil {
		return nil, err
	}

	var tpl bytes.Buffer
	if err := t.Execute(&tpl, nil); err != nil {
		return nil, err
	}

	return tpl.Bytes(), nil
}
