package repositories

import (
	"database/sql"
	"zsgo-db-dump/models"
)

type ZSGOData struct {
	db *sql.DB
}

func NewZSGOReposirory(db *sql.DB) *ZSGOData {
	return &ZSGOData{db}
}

func (z ZSGOData) GetDB(dumpRequest models.DumpRequest) (models.ZSGODatabase, error) {
	result, err := z.db.Query(
		`SELECT
		ws.uuid AS "database"
		FROM websites ws
		JOIN hostnames hn ON ws.id=hn.website_id
		JOIN companies c ON hn.fqdn=c.fqdn
		WHERE JSON_EXTRACT(c.license, "$.status.serial") = ?
		AND c.nif = ?`,
		dumpRequest.Serial,
		dumpRequest.NIF,
	)
	if err != nil {
		return models.ZSGODatabase{}, nil
	}
	defer result.Close()

	var dbName models.ZSGODatabase
	if result.Next() {
		if err := result.Scan(
			&dbName.Database,
		); err != nil {
			return models.ZSGODatabase{}, nil
		}
	}

	return dbName, nil
}

func (z ZSGOData) GetCompanyEmail(dumpRequest models.DumpRequest) (models.CompanyData, error) {
	result, err := z.db.Query(
		`SELECT name, email
		FROM companies
		WHERE JSON_EXTRACT(license, "$.status.serial") = ?
		AND nif = ?`,
		dumpRequest.Serial,
		dumpRequest.NIF,
	)
	if err != nil {
		return models.CompanyData{}, err
	}
	defer result.Close()

	var email sql.NullString
	var companyData models.CompanyData

	if result.Next() {
		if err := result.Scan(
			&companyData.Name,
			&email,
		); err != nil {
			return models.CompanyData{}, err
		}
	}

	if len(email.String) == 0 {
		result, err := z.db.Query(
			`SELECT c.name, u.email
			FROM users u
			JOIN company_has_users chu ON u.id = chu.user_id
			JOIN companies c ON c.id = chu.company_id
			WHERE JSON_EXTRACT(c.license, "$.status.serial") = ?
			AND c.nif = ?
			ORDER BY user_id 
			LIMIT 1;`,
			dumpRequest.Serial,
			dumpRequest.NIF,
		)
		if err != nil {
			return models.CompanyData{}, err
		}
		defer result.Close()

		if result.Next() {
			if err := result.Scan(
				&companyData.Name,
				&email,
			); err != nil {
				return models.CompanyData{}, err
			}
		}

	}

	if len(dumpRequest.Email) == 0 {
		companyData.Email = append(companyData.Email, email.String)
	} else {
		companyData.Email = append(companyData.Email, dumpRequest.Email)
	}

	return companyData, nil
}
