package models

type DumpRequest struct {
	Serial string `json:"serial"`
	NIF    string `json:"nif"`
	Email  string `json:"email"`
}
