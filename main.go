package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"zsgo-db-dump/config"
	"zsgo-db-dump/router"

	"github.com/gorilla/handlers"
)

func init() {
	logPath := filepath.Join(".", "logs")
	dumpsPath := filepath.Join(".", "db_dumps")
	if err := os.MkdirAll(logPath, os.ModePerm); err != nil {
		log.Fatal(err)
	}

	if err := os.MkdirAll(dumpsPath, os.ModePerm); err != nil {
		log.Fatal(err)
	}
}

func main() {
	config.Load()
	fmt.Printf("API listenning on port: %d | Environment: %s\n", config.Port, config.Env)
	r := router.Generate()
	headersOk := handlers.AllowedHeaders([]string{"Content-Type", "Authorization"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "POST"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", config.Port), handlers.CORS(headersOk, methodsOk, originsOk)(r)))
}
