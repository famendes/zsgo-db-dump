package controllers

import (
	"net/http"
	"zsgo-db-dump/responses"
)

func GetAPIStatus(w http.ResponseWriter, r *http.Request) {
	responses.JSON(w, http.StatusOK, "API up and running")
	return
}
