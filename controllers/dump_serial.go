package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"zsgo-db-dump/cleanup"
	"zsgo-db-dump/compressor"
	"zsgo-db-dump/database"
	"zsgo-db-dump/dump"
	"zsgo-db-dump/enums"
	"zsgo-db-dump/logger"
	"zsgo-db-dump/models"
	"zsgo-db-dump/repositories"
	"zsgo-db-dump/responses"
	sendmail "zsgo-db-dump/send_mail"
)

func DumpSerial(w http.ResponseWriter, r *http.Request) {
	standarLogger := logger.NewLogger()

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		standarLogger.Log(r, err.Error(), enums.LogError)
		responses.Err(w, http.StatusBadRequest, err)
		return
	}

	var dumpRequest models.DumpRequest
	if err := json.Unmarshal(body, &dumpRequest); err != nil {
		standarLogger.Log(r, err.Error(), enums.LogError)
		responses.Err(w, http.StatusBadRequest, err)
		return
	}

	db, err := database.Connect("zsgo")
	if err != nil {
		standarLogger.Log(r, err.Error(), enums.LogError)
		responses.Err(w, http.StatusInternalServerError, err)
		return
	}
	defer db.Close()

	rep := repositories.NewZSGOReposirory(db)
	zsgoDatabaseDump, err := rep.GetDB(dumpRequest)
	if err != nil {
		standarLogger.Log(r, err.Error(), enums.LogError)
		responses.Err(w, http.StatusInternalServerError, err)
		return
	}

	if len(zsgoDatabaseDump.Database) == 0 {
		standarLogger.Log(r, "", enums.LogAccess)
		responses.JSON(w, http.StatusUnprocessableEntity, struct {
			Err string `json:"error"`
		}{
			Err: "Não foi encontrada nenhuma base de dados pertencente ao serial e nif indicados.",
		})
		return
	}

	dumpedFile, err := dump.DumpDB(zsgoDatabaseDump.Database)
	if err != nil {
		standarLogger.Log(r, err.Error(), enums.LogError)
		responses.Err(w, http.StatusInternalServerError, err)
		return
	}

	compressedFile, err := compressor.Compress(dumpedFile, dumpRequest.NIF, zsgoDatabaseDump.Database)
	if err != nil {
		standarLogger.Log(r, err.Error(), enums.LogError)
		responses.Err(w, http.StatusInternalServerError, err)
		return
	}

	companyData, err := rep.GetCompanyEmail(dumpRequest)
	if err != nil {
		standarLogger.Log(r, err.Error(), enums.LogError)
		responses.Err(w, http.StatusInternalServerError, err)
		return
	}

	if err := sendmail.SendEmail(companyData, dumpRequest.NIF, compressedFile); err != nil {
		standarLogger.Log(r, err.Error(), enums.LogError)
		responses.Err(w, http.StatusInternalServerError, err)
		return
	}

	if err := cleanup.DeleteFiles([]string{dumpedFile, compressedFile}); err != nil {
		standarLogger.Log(r, err.Error(), enums.LogError)
	}

	responses.JSON(w, http.StatusOK, nil)
}
